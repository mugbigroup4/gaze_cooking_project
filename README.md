# Gaze Cooking Project
This repository contains the implementation of the team "Gaze Cooking" of the seminar "Multi User Gaze Based Interaction" of SS2017 at the Saarland University.

The task was to plan and implement an application which allows several users to interact with the use of eyetrackers.

## Eyetracking
For this project you need at least two eyetracking devices by [Pupil Labs](https://pupil-labs.com/) and we recommend to use one computer with the laterst Windows installation for each eyetracker.

## Setup

### Python 3
The Gaze Cooking Project is implemented in Python 3. For this please download and install it. Because several moduls are additionally needed we recomend to install Python with [Anaconda](https://www.anaconda.com/download/).

### Pupil Capture
The eyetracking devices run with Pupil's software called Pupil Capture which is included in the application package that can be downloaded [here](https://github.com/pupil-labs/pupil/releases/latest).

It is likely that one needs to install the [Windows drivers](https://drive.google.com/uc?export=download&id=0Byap58sXjMVfR0p4eW5KcXpfQjg) to run Pupil Capture properly.

For more information please read [Pupil's documentation pages](https://docs.pupil-labs.com/).

The following settings have to be applied to all running instances of Pupil Capture.

#### Pupil Remote Plugin
The Remote Plugin in the Capture application sends current gaze date over the network and finally allows the application to transfer the data to the webbrowser.
To activate the plugin, select in the dropdown menu the line "Pupil Remote".  On the bottom of the right UI panel a new section will be opened. Usually the port ```50020``` is selected as default, if not set it to.

#### Pupil Surface Tracking Plugin
With the Surface Tracking Plugin Pupil detects surfaces by scanning the scene from the world camera for markers.
To define surfaces the markers have to be visible during the setup.

##### Defining Surfaces

The Gaze Cooking Project uses 8 markers that are visible on the UI.
- Start the server application by running ```python startServer.py``` on the computer ("server machine") where Python 3 is installed and which is connected to the screen where the actual application should be visible. 
- Open the webrowser and open ```http://127.0.0.1:5000/```. The UI with the used markers should appear.
- On each running instance of Pupil Capture select the "Surface Tracker" Plugin from the "Open Plugin" dropdown.
- Now place the eyetracker in a position such the world camera recognizes all 8 markers.
- Save these surface by clicking "add surface". After that this procedure does not have to be repeated again.
- Finally rename the added surface to ```screen_0```.

### Running the Application

After finishing the previos steps the Gaze Cooking application can be started.

The Gaze Cooking Project is able to let 4 users use the application with one eyetracking device each at the same time while for debugging eyetracking input can be replaced by the curser.

### Running the Application with Eyetrackers only

- plug in all eyetrackers into each machine and run Pupil capture
- be sure that all machines are connected to the same network
- be sure that Pupil Remote is setup to port 50020 on each device
- For connecting each eyetracker one needs the address of each machine which can be tacken from the Pupil Remote UI at the point "Remote Address".
- on one compute ("server machine") where Python 3 is installed start the server by ```python startServer.py a1 a2 a3 ...``` (while a1 etc are the addresses of the machines that should be connected to the server) from the root directory of the repository
- The server is now hosting the interface at localhost. Open the webbrowser (on the server machine) and type ```http://127.0.0.1:5000/``` to open the UI. Now each eyetracker's gaze position is represented by a small circle.

### Running the Application with the cursor
To run the application with the cursor as input just launch the script without any arguments. When it is running you can type at any point the user id (0-3) to define for which user the gaze data should be faked by the mouse position. The id can be changed during the usage. Make sure to create the ids in ascending order, e.g. don't use id 2, when you haven't used 0 and 1 before.
The mouse coordinates on the screen are used, so you have to use the interface in full screen mode to match the mouse position.

#### Troubleshooting
- If the eyetrackers are not found in the network maybe the server's or the client's Firewall is blocking incoming or outgoing traffic. Disable the Firewall to get rid of this problem or open the ports manually.
- If an eyetracker's does not follow the trace where the user is actually looking at maybe a calibration with pupil capture can improve the gaze quality.

