/**
 * INSPINIA - Responsive Admin Theme
 *
 */


/**
 * pageTitle - Directive for set Page title - mata title
 */
function pageTitle($rootScope, $timeout) {
    return {
        link: function (scope, element) {
            var listener = function (event, toState, toParams, fromState, fromParams) {
                // Default title - load on Dashboard 1
                var title = 'Team cooking | Multi User Gaze-Based Interaction Seminar ';
                // Create your own title pattern
                if (toState.data && toState.data.pageTitle) title = 'INSPINIA | ' + toState.data.pageTitle;
                $timeout(function () {
                    element.text(title);
                });
            };
            $rootScope.$on('$stateChangeStart', listener);
        }
    }
}


/**
 * iboxTools - Directive for iBox tools elements in right corner of ibox
 */
function iboxTools($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.children('.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            },
                // Function for close ibox
                $scope.closebox = function () {
                    var ibox = $element.closest('div.ibox');
                    ibox.remove();
                }
        }
    };
}

/**
 * iboxTools with full screen - Directive for iBox tools elements in right corner of ibox with full screen option
 */
function iboxToolsFullScreen($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools_full_screen.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.children('.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
            // Function for close ibox
            $scope.closebox = function () {
                var ibox = $element.closest('div.ibox');
                ibox.remove();
            };
            // Function for full screen
            $scope.fullscreen = function () {
                var ibox = $element.closest('div.ibox');
                var button = $element.find('i.fa-expand');
                $('body').toggleClass('fullscreen-ibox-mode');
                button.toggleClass('fa-expand').toggleClass('fa-compress');
                ibox.toggleClass('fullscreen');
                setTimeout(function () {
                    $(window).trigger('resize');
                }, 100);
            }
        }
    };
}

function wizard() {
    var scope;

    return {
        restrict: "A",
        controller: function ($scope) {
            scope = $scope;
        },
        compile: function ($element) {
            $element.wrapInner('<div class="steps-wrapper">')
            var steps = $element.children('.steps-wrapper').steps({
                onStepChanging: function (event, currentIndex, newIndex) {
                    return scope.ctrl.onStepChanging(event, currentIndex, newIndex);
                },
                onFinished: function (event, currentIndex) {
                    scope.ctrl.finishedWizard(event, currentIndex);
                },
                labels: {
                    finish: "<div class='gaze-click'><i class=\"fa fa-cutlery\"></i> Start cooking</div>",
                    next: "<div class='gaze-click'>Next</div>",
                    loading: "Loading ..."
                },
                forceMoveForward: true,
                enableKeyNavigation: false,
                transitionEffect: 'slideLeft',
                transitionEffectSpeed: 700,
            });
        }
    };
}

function health() {
    return {
        restrict: 'E',
        scope: {
            pointers: '=',
            id: '@'
        },
        templateUrl: 'views/common/health.html',
        link: function (scope) {
            scope.$watch("pointers.length",function(newValue,oldValue) {
                scope.pointer = scope.pointers[scope.id];
            }, true);
        }
    };
}


/**
 *
 * Pass all functions into module
 */
angular
    .module('inspinia')
    .directive('pageTitle', pageTitle)
    .directive('iboxTools', iboxTools)
    .directive('wizard', wizard)
    .directive('health', health)
    .directive('iboxToolsFullScreen', iboxToolsFullScreen)
    .filter('millisecondsToDateTime', [function () {
        return function (milliseconds) {
            return new Date(1970, 0, 1).setMilliseconds(milliseconds);
        };
    }]);
