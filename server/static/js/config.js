function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/index/selection");

    $stateProvider
        .state('index', {
            abstract: true,
            url: "/index",
            templateUrl: "views/common/content.html",
        })
        .state('index.selection', {
            url: "/selection",
            templateUrl: "views/selection.html"
        })
        .state('index.cooking', {
            url: "/cooking",
            templateUrl: "views/cooking.html"
        })
}
angular
    .module('inspinia')
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });
