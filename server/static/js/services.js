function DataService() {
    this.users = [];
    this.recipe = null;

    this.getUsers = function () {
        return this.users;
    }

    this.setUsers = function (users) {
        this.users = users;
    }

    this.getRecipe = function () {
        return this.recipe;
    }

    this.setRecipe = function (recipe) {
        this.recipe = recipe;
    }
}

function PointerService($interval) {
    var self = this;
    this.pointers = [];
    this.clickPointer = undefined;

    this.getPointer = function(id) {
        if(!self.pointers[id]) {
            self.pointers[id] = {
                id: id,
                name: '',
                cssBackground: 'pointer-' + id + '-background',
                cssBorder: 'pointer-' + id + '-border',
                x: 0,
                y: 0,
                element: null,
                timeOnElement: 0,
                lastUpdate: -1,
                invalidData: 0,
                ok: false,
                time: -1
            };
            console.log("Created new pointer with id " + id, self.pointers[id]);
        }

        return self.pointers[id];
    };

    this.getClickPointer = function() {
        return self.clickPointer;
    }

    this.setClickPointer = function(pointer) {
        self.clickPointer = pointer;
    }

    this.updateHealthStatus = function () {
        for (var i = 0; i < self.pointers.length; i++) {
            var pointer = self.pointers[i];
            if (pointer.lastUpdate > 0) {
                pointer.time = (new Date().getTime() - pointer.lastUpdate) / 1000;
                pointer.ok = pointer.time < 4;
            }
        }
    };

    $interval(this.updateHealthStatus, 500);
}

angular.module('inspinia')
    .service('DataService', DataService)
    .service('PointerService', ['$interval', PointerService])
    .factory('socket', function ($rootScope) {
        var socket = io.connect();
        return {
            on: function (eventName, callback) {
                socket.on(eventName, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
            },
            emit: function (eventName, data, callback) {
                socket.emit(eventName, data, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        if (callback) {
                            callback.apply(socket, args);
                        }
                    });
                })
            }
        };
    });