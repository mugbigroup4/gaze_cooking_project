function MainCtrl(socket, $timeout, $interval, pointerService) {
    var self = this;
    this.pointers = pointerService.pointers;
    this.scrollInterval = undefined;
    this.scrollingPointer = undefined;

    socket.on('connect', function () {
        console.log("Websocket connected");
    });

    socket.on('gaze-position', function (data) {
        var pointer = pointerService.getPointer(data.gaze);

        if (pointer && data.x < $(window).width() && data.x >= 0 && data.y >= 0 && data.y < $(window).height()) {
            self.updateGaze(pointer, data);
        } else {
            //console.log('Invalid data', data, pointer, $(window).width(), $(window).height());
            if (pointer) {
                pointer.invalidData++;
            }
        }
    });

    this.getPositionAtCenter = function (element) {
        var data = element.getBoundingClientRect();
        return {
            x: data.left + data.width / 2,
            y: data.top + data.height / 2
        };
    };

    this.getGazeElement = function (gazePoint) {
        var elements = document.getElementsByClassName("gaze-click");
        var nearElements = [];

        //Save all near elements
        for (var i = 0; i < elements.length; i++) {
            var element = elements[i];
            var elemPosition = self.getPositionAtCenter(element);
            var distance = Math.sqrt(
                Math.pow(elemPosition.x - gazePoint.x, 2) +
                Math.pow(elemPosition.y - gazePoint.y, 2)
            );
            if (distance < 150) {
                element.distance = distance;
                element.centerPosition = elemPosition;
                nearElements.push(element);
            }
        }

        if (nearElements.length < 1) {
            return null;
        }

        nearElements.sort(function (a, b) {
            return a.distance - b.distance;
        });
        var targetElement = nearElements[0];
        gazePoint.x = targetElement.centerPosition.x;
        gazePoint.y = targetElement.centerPosition.y;

        return targetElement;
    };

    this.updateGaze = function (pointer, gazePoint) {
        var gazeElement = self.getGazeElement(gazePoint);
        var pointerJElement = angular.element(pointer.element);

        pointer.x = gazePoint.x;
        pointer.y = gazePoint.y;
        pointer.lastUpdate = new Date().getTime();

        if (gazeElement == null) {
            if (pointerJElement != null) {
                pointerJElement.removeClass("activeGaze");
                pointer.element = null;
            }
            angular.element('#pointer-' + pointer.id + ' .circle').removeClass("circle-animated");

            self.checkScroll(pointer, gazePoint);
        } else {
            self.stopScrolling(pointer);

            if (pointer.element != null && pointer.element == gazeElement) {
                pointerJElement.addClass("activeGaze");
                angular.element('#pointer-' + pointer.id + ' .circle').addClass("circle-animated");
                if (pointer.timeOnElement < Date.now() - 2000) {
                    //Don't trigger this button again until it has been leaved
                    pointer.timeOnElement = Infinity;
                    $timeout(function () {
                        pointerService.setClickPointer(pointer);
                        pointerJElement.trigger('click');
                    }, 0);
                }
            } else {
                if (pointerJElement != null) {
                    pointerJElement.removeClass("activeGaze");
                }
                angular.element('#pointer-' + pointer.id + ' .circle').removeClass("circle-animated");
                pointer.element = gazeElement;
                pointer.timeOnElement = Date.now();
            }
        }
    }

    this.checkScroll = function (pointer, gazePoint) {
        var elements = $(".wizard > .content:has(#vertical-timeline:visible)");
        if (elements.length > 0) {
            var scrollElement = elements[0];
            var pos = scrollElement.getBoundingClientRect();
            var scrollBoxHeight = 150;
            //Gaze point in element
            if (gazePoint.x > pos.left && gazePoint.x <= pos.left + pos.width && gazePoint.y > pos.top && gazePoint.y <= pos.top + pos.height) {
                //Top scroll box, scroll up
                if (gazePoint.y <= pos.top + scrollBoxHeight) {
                    self.startScrolling(pointer, elements, -1);
                    //Skip stop scrolling
                    return;
                } else if (gazePoint.y > pos.top + pos.height - scrollBoxHeight) {
                    //Bottom scroll box, scroll down
                    self.startScrolling(pointer, elements, +1);
                    //Skip stop scrolling
                    return;
                }
            }
        }

        self.stopScrolling(pointer);
    }

    this.startScrolling = function (pointer, elements, direction) {
        if (!angular.isDefined(self.scrollInterval)) {
            self.scrollingPointer = pointer;
            self.scrollInterval = $interval(function () {
                elements.animate({scrollTop: (elements.scrollTop() + direction * 20)}, 100);
            }, 102);
        }
    }

    this.stopScrolling = function (pointer) {
        if (angular.isDefined(self.scrollInterval)) {
            if (self.scrollingPointer.id == pointer.id) {
                self.scrollingPointer = undefined;
                $interval.cancel(self.scrollInterval);
                self.scrollInterval = undefined;
            }
        }
    }
}

function SelectionCtrl($scope, $location, dataService, pointerService) {
    var self = this;
    this.users = [
        {name: "Sven", skill: "Beginner", img: "img/Sven.jpg", task: 0, id: 0},
        {name: "Gerrit", skill: "Novice", img: "img/Gerrit.jpg", task: 0, id: 1},
        {name: "Daniela", skill: "Pro cook", img: "img/Daniela.jpg", task: 0, id: 2}
    ];
    this.selectedUsers = [];
    this.numSelectedUsers = function () { // returns the number of the selected users
        return this.selectedUsers.length;
    }
    this.addSelectedUser = function (user) {
        if (!this.selectedUsers.includes(user)) {
            var pointer = pointerService.getClickPointer();
            pointer.user = user;
            user.pointer = pointer;

            for (var i = 0; i < this.selectedUsers.length; i++) {
                if (this.selectedUsers[i].pointer.id == pointer.id) {
                    this.selectedUsers[i].pointer = undefined;
                    this.selectedUsers.splice(i, 1);
                    break;
                }
            }
            this.selectedUsers.push(user);
        } else {
            pointerService.getClickPointer().user = undefined;
            delete user.pointer;
            // find the index of the user to be removed in the array
            for (var i = 0; i < this.selectedUsers.length; i++) {
                if (this.selectedUsers[i].id == user.id) {
                    // now we found the index next we have to remove the element at this index
                    this.selectedUsers.splice(i, 1);
                    break;
                }
            }
        }
    }
    this.finishedWizard = function (event, currentIndex) {
        dataService.setRecipe(this.selectedRecipe);
        dataService.setUsers(this.selectedUsers);
        $location.path("/index/cooking");
        $scope.$apply();
    }
    this.onStepChanging = function (event, currentIndex, newIndex) {
        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex) {
            return true;
        }
        if (currentIndex == 0 && this.selectedUsers.length < 2) {
            if (this.selectedUsers.length > 0) {
                //Remove class and reapply to restart animation
                $('#userselectcounter').removeClass('animated');
                $('#userselectcounter').removeClass('wobble');
                setTimeout(
                    function () {
                        $('#userselectcounter').addClass('animated');
                        $('#userselectcounter').addClass('wobble');
                    }, 1);
            } else {
                //Remove class and reapply to restart animation
                $('.wrapper .col-lg-3').removeClass('animated');
                $('.wrapper .col-lg-3').removeClass('pulse');
                setTimeout(
                    function () {
                        $('.wrapper .col-lg-3').addClass('animated');
                        $('.wrapper .col-lg-3').addClass('pulse');
                    }, 1);
            }

            return false;
        }
        if (currentIndex == 1 && !this.selectedRecipe) {
            //Remove class and reapply to restart animation
            $('.notes li').removeClass('animated');
            $('.notes li').removeClass('swing');
            setTimeout(
                function () {
                    $('.notes li').addClass('animated');
                    $('.notes li').addClass('swing');
                }, 1);

            return false;
        }

        return true;
    }

    // recepie sourc: http://www.chefkoch.de/rezepte/1548041261407953/Milky-Way-Kekse.html
    this.recipes = [
        {
            name: "Milky Way cookies",
            img: "img/recipe1.jpg",
            steps: [{
                description: "Stear 250g fat, 200g sugar, 1 package vanilla sugar and the pinch of salt until every thing is creamy.",
                number: 1,
                timer: 0,
                dependent: []
            }, {
                description: "Add two eggs to the sugar fat mixture and stir.",
                number: 2,
                timer: 0,
                dependent: [1]
            }, {
                description: "Mix 420g flour, 2 tsp. cocoa and 2 tsp. baking powder",
                number: 3,
                timer: 0,
                dependent: []
            }, {
                description: "Add the flour mix to the sugar egg mix",
                number: 4,
                timer: 0,
                dependent: [2, 3]
            }, {
                description: "Dice 4 pieces of Milky Way and add with 25g almonds under the dough and knead.",
                number: 5,
                timer: 0,
                dependent: [4]
            }, {
                description: "Cover the backplate with baking paper.",
                number: 6,
                timer: 0,
                dependent: []
            }, {
                description: "Place 10 dough piles on the plate and press them a little flat.",
                number: 7,
                timer: 0,
                dependent: [5, 6]
            }, {
                description: "Heat the oven to 200 degrees Celcius.",
                number: 8,
                timer: 0,
                dependent: []
            }, {
                description: "Put the plate in the oven for 10-12 minutes.",
                number: 9,
                timer: 10,
                dependent: [7, 8]
            }, {description: "Decorate with couverture as desired.", number: 10, timer: 0, dependent: [9]}],
            description: "A recipe for Milky Way cookies. The amount is enough for 2 to 3 backing plates."
        },

        {
            name: "Fruit Salad",
            img: "img/recipe2.jpg",
            steps: [{
                description: "Wash 1 pint of strawberries, hull and slice them.",
                number: 1,
                timer: 0,
                dependent: []
            }, {
                description: "Halve 1 pound of seedless grapes.",
                number: 2,
                timer: 0,
                dependent: []
            }, {
                description: "Peel and slice 3 kiwis",
                number: 3,
                timer: 0,
                dependent: []
            }, {
                description: "Slice 3 bananas",
                number: 4,
                timer: 0,
                dependent: []
            }, {
                description: "In a large bowl, combine the strawberries, grapes, kiwis, and bananas.",
                number: 5,
                timer: 0,
                dependent: [1, 2, 3, 4]
            }, {
                description: "Gently mix in 1 (21 ounce) can peach pie filling.",
                number: 6,
                timer: 0,
                dependent: [5]
            }, {
                description: "Chill for 1 hour before serving.",
                number: 7,
                timer: 60,
                dependent: [6]
            }],
            description: "A recipe for an easy fruit salad."
        },

        {
            name: "Roasted Vegetables",
            img: "img/recipe3.jpg",
            steps: [{
                description: "Preheat oven to 475 degrees F (245 degrees C).",
                number: 1,
                timer: 0,
                dependent: []
            }, {
                description: "Cube 1 small butternut squash.",
                number: 2,
                timer: 0,
                dependent: []
            }, {
                description: "Seed and dice 2 red bell peppers.",
                number: 3,
                timer: 0,
                dependent: []
            }, {
                description: "Peel and cube 1 sweet potato.",
                number: 4,
                timer: 0,
                dependent: []
            }, {
                description: "Cube 3 Yukon Gold potatoes.",
                number: 5,
                timer: 0,
                dependent: []
            }, {
                description: "In a large bowl, combine the squash, red bell peppers, sweet potato, and Yukon Gold potatoes.",
                number: 6,
                timer: 0,
                dependent: [2, 3, 4, 5]
            }, {
                description: "Quarter one red onion.",
                number: 7,
                timer: 0,
                dependent: []
            }, {
                description: "Separate the red onion quarters into pieces, and add them to the mixture.",
                number: 8,
                timer: 0,
                dependent: [6, 7]
            }, {
                description: "In a small bowl, stir together 1 tablespoon chopped fresh thyme, 2 tablespoons chopped fresh rosemary, 1/4 cup olive oil, 2 tablespoons balsamic vinegar, salt and freshly ground black pepper. ",
                number: 9,
                timer: 0,
                dependent: []
            }, {
                description: "Toss the sauce with vegetables until they are coated.",
                number: 10,
                timer: 0,
                dependent: [8, 9]
            }, {
                description: "Spread the vegetables withe sauce on a large roasting pan.",
                number: 11,
                timer: 0,
                dependent: [10]
            }, {
                description: "Roast for 35 to 40 minutes in the preheated oven, stirring every 10 minutes, or until vegetables are cooked through and browned.",
                number: 12,
                timer: 35,
                dependent: [1, 11]
            }],
            description: "A casserole dish of seasonal vegetables that is so easy to prepare."
        }

    ];

    this.selectedRecipe = null;
}

function CookingCtrl($interval, dataService, pointerService, SweetAlert) {
    var self = this;
    this.recipe = dataService.getRecipe();
    // Users have: (name, skill, img, task)
    this.Users = dataService.getUsers();
    // Users waiting for a new available task
    this.waitingUsers = [];
    // list of users looking at the screen.
    // TODO for now this is just equal to the users. This still has to be adapted.
    this.watchingUsers = this.Users;//[];

    // All the tasks to handle
    // Tasks have: (description, number, dependencies)
    this.finishedTasks = [];
    this.currentTasks = [];
    this.untouchedTasks = this.recipe.steps.slice();
    this.timerTasks = [];

    this.taskButtonClicked = function () {
        var user = pointerService.getClickPointer().user;
        if (user.task != 0) {
            self.finishTask(self.getTask(user.task), user);
            self.nextTask(user);
        }
    }

    // Functions
    // this function requires that a finish task has been called earlier, so that a user has no task currently.
    // @User: the User who wants a new task
    this.nextTask = function (user) {
        var taskGiven = false;
        var i;
        for (i = 0; i < this.untouchedTasks.length; i++) {
            if (!this.isTaskDependent(this.untouchedTasks[i])) {
                // assigne the task to the user
                var task = this.untouchedTasks[i];
                user.task = task.number;
                // remove the task from the untouchedTasks list
                this.untouchedTasks.splice(i, 1);
                // put the task in the currentTasks list
                this.currentTasks.push(task);
                taskGiven = true;
                break;
            }
        }
        if (!taskGiven) {
            // add the user to the waitingUsers list
            if (this.waitingUsers.indexOf(user) == -1) {
                this.waitingUsers.push(user);
            }
        } else {
            // check if the user is in the waiting users list. If yes remove him.
            if (this.waitingUsers.indexOf(user) > -1) {
                this.waitingUsers.splice(this.waitingUsers.indexOf(user), 1);
            }
        }
    }

    this.checkTimerTasks = function() {
        for(var i = 0; i < self.timerTasks.length; i++) {
            var task = self.timerTasks[i];
            task.remainingTime = task.finishTime - new Date().getTime();
            if(task.remainingTime <= 0) {
                self.finishTimerTask(task);
            }
        }
    }

    this.finishTimerTask = function(task) {
        var index = this.timerTasks.indexOf(task);
        // remove the task from the list
        this.timerTasks.splice(index, 1);
        self.finishedTasks.push(task);
        self.assignNewTasks();
    }

    this.assignNewTasks = function() {
        // for all users in the waiting list call the nextTask function
        var i;
        for (i = 0; i < this.waitingUsers.length; i++) {
            this.nextTask(this.waitingUsers[i]);
        }

        if (this.isRecipeDone()) {
            SweetAlert.swal({
                title: "Dinner is ready!",
                text: "Did you like the recipe?",
                type: "success",
                confirmButtonText: '<i class="fa fa-thumbs-up"></i> Yes',
                cancelButtonText: '<i class="fa fa-thumbs-down"></i> No',
                confirmButtonClass: 'gaze-click',
                cancelButtonClass: 'gaze-click',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                animation: false,
                customClass: 'animated bounceInDown'
            });
        }}

    // this function finishes a task
    // @task : the task to finish
    this.finishTask = function (task, user) {
        // free the user of the task
        user.task = 0;
        // find the index of the current task in the current list
        var index = this.currentTasks.indexOf(task);
        // remove the task from the current list
        this.currentTasks.splice(index, 1);

        if (task.timer > 0) {
            task.finishTime = new Date().getTime() + (1000 * 60 * task.timer);

            self.timerTasks.push(task);
        } else {
            // add the task to the finished list
            self.finishedTasks.push(task);
        }

        self.assignNewTasks();
    }

    // checks whether a task is dependent of other unfinished tasks
    // @ Task: the task to check
    // @boolean : indication if the task is available
    this.isTaskDependent = function (task) {
        var dependentOn = task.dependent;
        var i;
        for (i = 0; i < this.currentTasks.length; i++) {
            var x = this.currentTasks[i].number;
            if (dependentOn.indexOf(x) > -1) {
                return true;
            }
        }
        for (i = 0; i < this.untouchedTasks.length; i++) {
            var x = this.untouchedTasks[i].number;
            if (dependentOn.indexOf(x) > -1) {
                return true;
            }
        }
        for (i = 0; i < this.timerTasks.length; i++) {
            var x = this.timerTasks[i].number;
            if (dependentOn.indexOf(x) > -1) {
                return true;
            }
        }

        return false;
    }

    // cheks if all the tasks of the recipe are done.
    this.isRecipeDone = function () {
        return this.currentTasks.length == 0 && this.untouchedTasks.length == 0 && this.timerTasks == 0;
    }

    // Returns the User who currently works on the given task
    // @task: The task the user currently works on
    this.getUserForTask = function (task) {
        var user = null;
        var i;
        for (i = 0; i < this.Users.length; i++) {
            if (task.number == this.Users[i].task) {
                user = this.Users[i];
            }
        }
        return user;
    }

    this.getUserIdForTask = function (task) {
        var user = this.getUserForTask(task);
        return user.id;
    }

    // get a current task by the task number
    //@ taskNumber: the number of the task required
    //@ return the current task with the respective number
    this.getTask = function (taskNumber) {
        for (i = 0; i < this.currentTasks.length; i++) {
            if (this.currentTasks[i].number == taskNumber) {
                return this.currentTasks[i];
            }
        }

        return null;
    }

    this.helpActivation = function () {
        var userName = pointerService.getClickPointer().user.name;
        if (!userName) {
            userName = 'Somebody';
        }
        SweetAlert.swal({
            title: userName + " needs your help!",
            type: "info",
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            confirmButtonClass: 'gaze-click',
            animation: true,
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false
        });
    }

    var i;
    for (i = 0; i < this.Users.length; i++) {
        this.nextTask(this.Users[i]);
    }

    $interval(self.checkTimerTasks, 400);
}


angular
    .module('inspinia')
    .controller('MainCtrl', ['socket', '$timeout', '$interval', 'PointerService', MainCtrl])
    .controller('SelectionCtrl', ['$scope', '$location', 'DataService', 'PointerService', SelectionCtrl])
    .controller('CookingCtrl', ['$interval', 'DataService', 'PointerService', 'SweetAlert', CookingCtrl])