/**
 * INSPINIA - Responsive Admin Theme
 *
 */
(function () {
    angular.module('inspinia', [
        'ui.router',                    // Routing
        'ui.bootstrap',                 // Ui Bootstrap
        'oitozero.ngSweetAlert',
        'btford.socket-io'
    ])
})();

