from flask import Flask
from flask import redirect
from flask_socketio import SocketIO, emit
import win32gui
import sys
import SurfaceTracking
import threading
from pynput.keyboard import Key, Listener

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
use_mouse = False
mouse_pointer_id = 0
thread = None


class EyetrackerThread(threading.Thread):
    def __init__(self, tracker_id, surface_tracker):
        threading.Thread.__init__(self)

        self.tracker_id = tracker_id
        self.surface_tracker = surface_tracker

    def run(self):

        print("started Thread for tracker {}".format(self.tracker_id))

        while True:
            socketio.sleep(0.03)
            x, y, surface_name = self.surface_tracker.get_tracking_data()

            if x < 0 or y == 0:
                print("Negative coordinates", x, y)
                continue

            print("Received values (x, y, surface_name): ", x, y, surface_name)
            cur_screen_id = screen_ids[surface_name]

            print(
                "Tracker " + str(tracker_id) + " X:" + str(x) + " Y: " + str(y) + " on surface " + str(
                    cur_screen_id))

            socketio.emit("gaze-position", {'x': x, 'y': y, 'gaze': tracker_id, 'screen': cur_screen_id})


class KeyListener(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def on_press(self, key):
        try:
            new_id = int(key.char)

            global mouse_pointer_id
            mouse_pointer_id = new_id
            print("New id: ", mouse_pointer_id)
        except (ValueError, AttributeError):
            pass

    def run(self):
        print("started key thread")

        while True:
            # Collect events until released
            with Listener(on_press=self.on_press) as listener:
                listener.join()


@app.route('/')
def hello_world():
    return redirect("/static/index.html#/index/selection", code=302)


def mouse_thread():
    key_thread = KeyListener()
    key_thread.start()
    print("Mouse thread started")
    while True:
        socketio.sleep(0.03)
        position = win32gui.GetCursorPos()
        socketio.emit('gaze-position',
                      {'x': position[0], 'y': position[1], 'screen': 0, 'gaze': mouse_pointer_id})


def mouse_id_thread():
    global mouse_pointer_id
    while True:
        print(sys.stdin.read(1))
        mouse_pointer_id = input("Enter pointer id: ")
        print("New mouse pointer id", mouse_pointer_id)


def pupil_thread():
    print("Starting pupil threads")

    for cur_tracker_id, cur_surface_tracker in trackers.items():
        eyetracer_thread = EyetrackerThread(cur_tracker_id, cur_surface_tracker)
        eyetracer_thread.start()


@socketio.on('connect')
def connect():
    print("Incoming websocket connection")
    global thread
    if thread is None:
        thread = socketio.start_background_task(target=mouse_thread) if use_mouse else socketio.start_background_task(
            target=pupil_thread)
    print("Websocket connected")


@socketio.on_error()  # Handles the default namespace
def error_handler(error):
    print("Websocket error:", error)


if __name__ == '__main__':
    trackers = {}

    # each eyetracker gets a tracker id starting at 0
    tracker_id = 0

    # each screen has an id depending the defined name in pupil capture
    screen_ids = {"screen_0": 0, "screen_1": 1, "screen_2": 2, "screen_3": 3, "screen_4": 4}

    # read the addresses of the eyetrackers
    if len(sys.argv) < 2:
        print("No eye tracker port supplied, using mouse input")
        use_mouse = True
    else:
        # save the port and create a surfaceTracker for each eye tracker
        for i in range(1, len(sys.argv)):
            # creating a new surface tracker
            surface_tracker = SurfaceTracking.SurfaceTracker("")
            surface_tracker.setup_socket(sys.argv[i], "50020")
            print("Socket setup success for tracker with id {} at address {}".format(str(tracker_id), sys.argv[i]))

            # save the surface tracker in a dictionary
            trackers[tracker_id] = surface_tracker
            tracker_id += 1

        print("Starting Server for {} eye trackers".format(str(len(trackers))))

    socketio.run(app)
    print("Finished")
