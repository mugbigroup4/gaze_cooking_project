

class TrackingSmoother:

    DEBUG = False

    def __init__(self, buffer_size=5, movement_threshold=20):

        self.data_buffer = []
        self.max_size = buffer_size

        self.last_mean = 0
        self.movement_threshold = movement_threshold

    def add_value(self, value):

        self.data_buffer.append(value)

        if len(self.data_buffer) > self.max_size:
            self.data_buffer.pop(0)

    def get_mean(self):
        return sum(self.data_buffer) / len(self.data_buffer)

    def slide_to(self, new_value):
        self.data_buffer = [new_value]
        self.last_mean = new_value

    def smooth(self, new_value):

        # add the value to the buffer
        self.add_value(new_value)

        # check if the mean changed much
        cur_mean = self.get_mean()

        # mean changed much
        if TrackingSmoother.DEBUG:
            print("Dist: {}".format(str(abs(cur_mean - self.last_mean))))

        if abs(cur_mean - self.last_mean) > self.movement_threshold:
            self.slide_to(new_value)
            if TrackingSmoother.DEBUG:
                print("Sliding!")
            return new_value
        else:
            self.last_mean = cur_mean
            if TrackingSmoother.DEBUG:
                print("Calm!")
            return cur_mean



