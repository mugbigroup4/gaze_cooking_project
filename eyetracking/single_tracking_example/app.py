#!/usr/bin/env python
from flask import Flask, render_template, session
from flask_socketio import SocketIO, emit, disconnect
import SurfaceTracking

# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None

surface_tracker = SurfaceTracking.SurfaceTracker("screen", True)

# here for each camera instance a sockets needs to be setup
surface_tracker.setup_socket()


def background_thread():
    """Example of how to send server generated events to clients."""
    count = 0
    while True:
        # socketio.sleep(10)
        # count += 1
        # socketio.emit('my_response',
        #               {'data': 'Server generated event', 'count': count},
        #               namespace='/test')

        # here for each eye tracker instance one needs to listen for the coordinates - use timeouts to not block other trackers
        x, y = surface_tracker.get_tracking_data()
        print("X:" + str(x) + " Y: " + str(y))

        # instead of "my_response" one can give each camera instance a unique id
        socketio.emit('my_response', {'data_x': x, 'data_y': y}, namespace='/test')


@app.route('/')
def index():
    return render_template('index.html')


@socketio.on('my_event', namespace='/test')
def test_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response', {'data': message['data'], 'count': session['receive_count']})


@socketio.on('disconnect_request', namespace='/test')
def disconnect_request():
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': 'Disconnected!', 'count': session['receive_count']})
    disconnect()


@socketio.on('connect', namespace='/test')
def test_connect():
    global thread
    if thread is None:
        thread = socketio.start_background_task(target=background_thread)

    emit('my_response', {'data': 'Connected', 'count': 0})

    # for i in range(10):
    #     x, y = surface_tracker.get_tracking_data()
    #     emit('my_response', {'data': str(x) + ' - ' + str(y), 'count': 0})


if __name__ == '__main__':
    socketio.run(app, debug=True)
