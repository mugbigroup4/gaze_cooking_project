
import zmq
from msgpack import loads
import subprocess as sp
from platform import system
from pymouse import PyMouse
import signal
import TrackingSmoothing


class SurfaceTracker:

    def __init__(self, surface_name="screen"):

        self.surface_name = surface_name

        self.x_smoother = TrackingSmoothing.TrackingSmoother(buffer_size=10, movement_threshold=3)
        self.y_smoother = TrackingSmoothing.TrackingSmoother(buffer_size=10, movement_threshold=3)

        self.socket = None

        # TODO: Here one has to input the resolution of the canvas object on the website... :(
        self.x_dim, self.y_dim = 1280, 615  # 1920, 1080 # self.get_screen_size()

        print("Screen Size = {} x {}".format(str(self.x_dim), str(self.y_dim)))

    def setup_socket(self, addr='127.0.0.1', req_port="50020"):

        context = zmq.Context()

        req = context.socket(zmq.REQ)
        req.connect("tcp://{}:{}".format(addr, req_port))

        # ask for the sub port
        req.send_string('SUB_PORT')

        print("Waiting for the sub port address")

        # handle the user's CTRL-C for aborting the server
        signal.signal(signal.SIGINT, signal.SIG_DFL)
        sub_port = req.recv_string()

        # open a sub port to listen to pupil
        self.socket = context.socket(zmq.SUB)
        self.socket.connect("tcp://{}:{}".format(addr, sub_port))
        self.socket.setsockopt_string(zmq.SUBSCRIBE, 'surface')

    def transform_coords_2_screen(self, raw_x, raw_y, smooth_x=0.5,  smooth_y=0.5):

        # smoothing out the gaze so the mouse has smoother movement
        # smooth_x += 0.35 * (raw_x - smooth_x)
        # smooth_y += 0.35 * (raw_y - smooth_y)

        raw_y = 1 - raw_y  # inverting y so it shows up correctly on screen
        raw_x *= int(self.x_dim)
        raw_y *= int(self.y_dim)

        x = self.x_smoother.smooth(raw_x)
        y = self.y_smoother.smooth(raw_y)

        return x, y
        # return raw_x, raw_y

    def get_tracking_data(self):

        # handle the user's CTRL-C for aborting the server
        # signal.signal(signal.SIGINT, signal.SIG_DFL)

        topic, msg = self.socket.recv_multipart()
        gaze_position = loads(msg, encoding='utf-8')

        gaze_on_screen = gaze_position['gaze_on_srf']

        if len(gaze_on_screen) > 0:
            # there may be multiple gaze positions per frame, so you could average them
            raw_x = sum([i['norm_pos'][0] for i in gaze_on_screen]) / len(gaze_on_screen)
            raw_y = sum([i['norm_pos'][1] for i in gaze_on_screen]) / len(gaze_on_screen)

            # or just use the most recent gaze position on the surface
            # raw_x, raw_y = gaze_on_screen[-1]['norm_pos']

            x, y = self.transform_coords_2_screen(raw_x, raw_y)

            return x, y, gaze_position['name']

        return -1, -1, ""
