1. start pupil capture
2. define the surface with the markers witht he surface tracking plugin and rename the surface to "screen"
3. remember at which ip address the eyetracking data will be sent using the pupil remote plugin (check the address behind "connect remotely") - the port number must be 50020 at the moment
4. do steps 1-3 for each eye tracking device (use one pupil capure instance per device)
5. start the startServer.py with the communication address of each eyetracker seperated by spaces - example: python startServer.py 172.16.53.38 172.134.53.31
6. open your web browser at 127.0.0.1:5000

Note: Firewall settings should allow the communication. Not sure if one has to enable network visibility or sth like that