$(document).ready(function() {
    
    
    function getPosition(el) {
          var xPosition = 0;
          var yPosition = 0;

        while (el) {
            xPosition += (el.offsetLeft - el.scrollLeft + el.clientLeft);
            yPosition += (el.offsetTop - el.scrollTop + el.clientTop);
            el = el.offsetParent;
        }
        
        return {
           
            x: xPosition,
            y: yPosition
        };
    } 

    var canvas = document.querySelector("#myCanvas");
    var context = canvas.getContext("2d");
    
    var tot_height = $(document).height();
    var tot_width =  $(document).width();
    
    canvas.width = tot_width;
    canvas.height = tot_height;

    var canvasPos = getPosition(canvas);
    
    var gazeX = 0;
    var gazeY = 0;
    
    var cyc_states = {};
    var color_list = ["#FF6A6A", "#6666FF", "#FFCC00", "#009933", "#FF00FF"];
    
    function update_cyc(port, x, y) { 
        
        context.clearRect(0, 0, canvas.width, canvas.height);
        
        var fin_x = Math.max(0, x);
        fin_x = Math.min(fin_x, tot_width);
        
        var fin_y = Math.max(0, y);
        fin_y = Math.min(fin_y, tot_height);
        
        cyc_states[port] = {x: fin_x, y:fin_y};
        console.log(cyc_states[port])
        
        draw_all_cycs();
    }
    
    function draw_cyc(x, y, color){
        context.beginPath();
        context.arc(x, y, 50, 0, 2 * Math.PI, true);
        context.globalAlpha = 0.5;
        context.fillStyle = color;
        context.fill();
    }
    
    function draw_all_cycs(){
        
        var i = 0;
        for (port in cyc_states){    
            x = cyc_states[port].x;
            y = cyc_states[port].y;
            draw_cyc(x, y, color_list[i]);
            i++;
        }
    }

    namespace = '/test';
    var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace);

    socket.on('tracker_data', function(msg) {
        //update_cyc(msg.data_x, msg.data_y, "#FF6A6A");
        update_cyc(msg.port, msg.data_x, msg.data_y);
    });

});