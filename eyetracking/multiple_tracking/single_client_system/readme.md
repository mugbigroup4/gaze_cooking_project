1. start pupil capture
2. define the surface with the markers witht he surface tracking plugin and rename the surface to "screen"
3. remember at which port the eyetracking data will be sent using the pupil remote plugin
4. do steps 1-3 for each eye tracking device (use several pupil capure instances)
5. start the startServer.py with the communication port of each eyetracker seperated by spaces - example: python startServer.py 50855 50856
6. open your web browser at 127.0.0.1:5000

Note: Will definitely crash when using more than 5 eyetrackers