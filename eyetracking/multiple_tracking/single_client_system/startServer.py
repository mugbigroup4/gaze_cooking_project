#!/usr/bin/env python

from flask import Flask, render_template, session
from flask_socketio import SocketIO, emit, disconnect
import SurfaceTracking
import sys

# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
thread = None


def background_thread():
    while True:
        for port, surface_tracker in trackers.items():
            x, y = surface_tracker.get_tracking_data()
            print("Tracker at port " + str(port) + " X:" + str(x) + " Y: " + str(y))

            # instead of "my_response" one can give each camera instance a unique id
            socketio.emit("tracker_data", {'data_x': x, 'data_y': y, 'port': port}, namespace='/test')


@app.route('/')
def index():
    return render_template('index.html')


@socketio.on('connect', namespace='/test')
# start the background thread
def test_connect():
    global thread
    if thread is None:
        thread = socketio.start_background_task(target=background_thread)

if __name__ == '__main__':
    trackers = {}

    if len(sys.argv) < 2:
        print("You have to enter at least one eye tracker port")

    else:

        # save the port and a surfaceTracker for each eye tracker
        for i in range(1, len(sys.argv)):

            surface_tracker = SurfaceTracking.SurfaceTracker("screen", True)
            surface_tracker.setup_socket('127.0.0.1', sys.argv[i])
            trackers[sys.argv[i]] = surface_tracker

        print("Starting Server for {} eye trackers".format(str(len(trackers))))

        socketio.run(app, debug=True)
